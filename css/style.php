<?php
    header("Content-type: text/css; charset: UTF-8");
?>


.nav-item a:hover{
background-color:#fff;
border-radius:8px;
}
.active{
    background-color:#fff;
border-radius:8px;   
}
.btn-blue{
    background-color:#364baf;
    box-shadow: 0 4px 4px rgb(0 0 0 / 25%);
    color:#fff;
}
.btn-blue:hover{
    background-color:#fff;
    box-shadow: 0 4px 4px rgb(0 0 0 / 25%);
    color:#364baf;
}
.btn-violet{
    background-color:#84199e;
    box-shadow: 0 4px 4px rgb(0 0 0 / 25%);
    color:#fff;
}
.btn-yellow{
    background-color:#ffeb00;
    box-shadow: 0 4px 4px rgb(0 0 0 / 25%);
    color:#fff;
}
.btn-violet:hover{
    color:#fff;
}
.btn-yellow:hover{
    color:#fff;
}