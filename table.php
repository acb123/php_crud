<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Crud</title>
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
   <link rel='stylesheet' type='text/css' href='css/style.php' />
    <!--<link rel="stylesheet" href="css/style.css">-->
</head>

<body>
      
<nav class="navbar navbar-expand-lg navbar-light bg-light shadow-lg">
                <a class="navbar-brand" href="#">ACB</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                <a class="nav-link" href="./index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                <a class="nav-link" href="#">Dashboard</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#">Login</a>
                </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
                </div>
                </nav>


    <?php require_once 'process.php'; ?>
    
    <div class="container mt-2">
         
         <a href="index.php" class="btn btn-blue">Back</a>

         <!--displaying session message-->
         <?php 
                     if(isset($_SESSION['message'])): ?>
                     <div class="text-center alert mt-2 alert-<?=$_SESSION['msg_type'] ?>">
                     <?php 
                      echo $_SESSION['message'];
                      unset($_SESSION['message']);
                     ?>
                     </div>
<?php endif; ?>
        
                <!--for displaying data inside table-->
                <?php 
                  $mysqli = new mysqli('localhost' , 'root', '' ,'crud')or die(mysqli_error($mysqli));

                  $result = $mysqli->query("SELECT * FROM data")or die($mysqli->error);
                ?>
               
                <div class="better mt-3">
                   

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Age</th>
                                <th scope="col">Designation</th>
                                <th colspan="2">Options</th>
                            </tr>
                        </thead>
                        <?php  
                           while($row = $result->fetch_assoc()):
                        ?>
                        <tbody>
                            <tr>
                                <!--getting the data from database-->
                                <td><?php echo $row['uname']; ?></td>
                                <td><?php echo $row['age']; ?></td>
                                <td><?php echo $row['designation']; ?></td>
                                
                                <!--passing the id for edit and delete buttons-->
                                <td><a href="index.php?edit=<?php echo $row['id']; ?>" class="btn btn-violet">Edit</a>
                                <a href="process.php?delete=<?php echo $row['id']; ?>" class="btn btn-yellow">Delete</a></td>
                            </tr>
                         </tbody>
                           <?php endwhile; ?>
                    </table>
                </div>
            </div>
        


</body>

</html>