<?php 
// Enable WP_DEBUG mode
define( 'WP_DEBUG', true );

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Disable display of errors and warnings 
define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', 0 );

session_start();

$id = 0;
$update = false;
$name = '';
$age = '';
$designation = '';

/*for connecting to database*/
$mysqli = new mysqli('localhost' , 'root' , '' , 'crud')or die(mysqli_error($mysqli_error));


/*for inserting the values to database*/
if(isset($_POST['save'])){
    $name = $_POST['username'];
    $age = $_POST['userage'];
    $designation = $_POST['userdesignation'];

    $mysqli->query("INSERT INTO data (uname,age,designation)VALUES('$name','$age', '$designation')") or
    die($mysqli->error);

    $_SESSION['message'] = "Record has been Saved";
    $_SESSION['msg_type'] = "success";

    header("location: table.php");
}

   /*for delete the records in the table*/
   if(isset($_GET['delete'])){
      $id = $_GET['delete'];
      $mysqli->query("DELETE FROM data WHERE id=$id")or die($mysqli->error());

      $_SESSION['message'] = "Record Has been deleted";
      $_SESSION['msg_type'] = "danger";
 
      header("location: table.php");
  }

  if(isset($_GET['edit'])){
      $id = $_GET['edit'];
      $update = true;
      $result = $mysqli->query("SELECT * FROM data WHERE id=$id")or die($mysqli->error());
      if (count($result)==1){
          $row = $result->fetch_array();
          $name = $row['uname'];
          $age = $row['age'];
          $designation = $row['designation'];
      }
  }

  if(isset($_POST['update'])){
      $id = $_POST['id'];
      $name = $_POST['username'];
      $age = $_POST['userage'];
      $designation = $_POST['userdesignation'];

      $mysqli->query("UPDATE data SET uname='$name', age='$age', designation='$designation' WHERE id=$id")or die($mysqli->error);

      $_SESSION['message'] = "Record has Been Updated";
      $_SESSION['msg_type'] = "primary";

      header('location: table.php');
  }
    
?>