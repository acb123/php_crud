<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Crud</title>
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
   <link rel='stylesheet' type='text/css' href='css/style.php' />
    <!--<link rel="stylesheet" href="css/style.css">-->
</head>

<body>
    
<nav class=" navbar navbar-expand-lg navbar-light bg-light shadow-lg">
                <a class="navbar-brand" href="#">ACB</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="./table.php">Dashboard</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#">Login</a>
                </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
                </div>
                </nav>


    <?php require_once 'process.php'; ?>
    
    <div class="container">
    <div class="row justify-content-center">
    <div class="col-md-6 mt-4 shadow-lg">
    <h4 class="text-center p-2">ADD EMPLOYEE DETAILS</h4>
         <!--displaying session message-->
         <?php 
                     if(isset($_SESSION['message'])): ?>
                     <div class="text-center alert mt-2 alert-<?=$_SESSION['msg_type'] ?>">
                     <?php 
                      echo $_SESSION['message'];
                      unset($_SESSION['message']);
                     ?>
                     </div>
<?php endif; ?>
         
        <form action="process.php" method="POST" name="form">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
            <div class="row mt-4" id="myform">
                    <div class="input-group col-sm-12 mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="addon-wrapping"><i class="fa fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" name="username" value="<?php echo $name; ?>" placeholder="Enter Name" id="user_name"
                            aria-label="Your Name" aria-describedby="addon-wrapping">
                    </div>

                    <div class="input-group col-sm-12 mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="addon-wrapping"><i class="fa fa-birthday-cake"></i></span>
                        </div>
                        <input type="text" class="form-control" name="userage" value="<?php echo $age; ?>"  placeholder="Enter Age" id="user_age"
                            aria-label="mail-id" aria-describedby="addon-wrapping">
                    </div>
                    <div class="input-group col-sm-12 mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="addon-wrapping"><i class="fa fa-briefcase"></i></span>
                        </div>
                        <input type="text" class="form-control" name="userdesignation" value="<?php echo $designation; ?>" placeholder="Enter Designation" id="user_designation"
                            aria-label="mail-id" aria-describedby="addon-wrapping">
                    </div>
                    <div class="col-sm-12 mt-2 button1 ">
                        <?php
                          if ($update == true): 
                        ?>
                        <button type="submit" class="btn btn-blue ml-2 mb-2" name="update">Update</button>
                        <?php else: ?>
                        <button type="submit" class="btn btn-blue ml-2 mb-2" name="save">Save</button>
                        <?php endif; ?>
                    </div>


                
               
            </div>
        </form>
        </div>
        </div>
    </div>


</body>

</html>